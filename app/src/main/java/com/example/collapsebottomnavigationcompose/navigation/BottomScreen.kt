package com.example.collapsebottomnavigationcompose.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector

sealed class BottomScreen(val route: String, val title: String, val icon: ImageVector) {
    object Home : BottomScreen("home", "Home", Icons.Default.Home)
    object Favorite : BottomScreen("favorite", "Favorite", Icons.Default.Favorite)
    object Search : BottomScreen("search", "Search", Icons.Default.Search)
    object Settings : BottomScreen("settings", "Settings", Icons.Default.Settings)
    object User : BottomScreen("user", "User", Icons.Default.Person)
}

val bottomNavigationItems = listOf(
    BottomScreen.Home,
    BottomScreen.Favorite,
    BottomScreen.Search,
    BottomScreen.Settings,
    BottomScreen.User,
)